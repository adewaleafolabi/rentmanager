# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table building (
  id                            bigint auto_increment not null,
  building_name                 varchar(255),
  building_address              varchar(255),
  constraint pk_building primary key (id)
);

create table building_facility (
  building_id                   bigint not null,
  facility_id                   bigint not null,
  constraint pk_building_facility primary key (building_id,facility_id)
);

create table customer (
  id                            bigint auto_increment not null,
  first_name                    varchar(255),
  middle_name                   varchar(255),
  last_name                     varchar(255),
  phone_number                  varchar(255),
  email                         varchar(255),
  constraint pk_customer primary key (id)
);

create table facility (
  id                            bigint auto_increment not null,
  facility_value                varchar(255),
  constraint pk_facility primary key (id)
);

create table login_audit_trail (
  id                            bigint auto_increment not null,
  event_type                    varchar(17),
  event_date                    datetime(6),
  ip_address                    varchar(255),
  username                      varchar(255),
  user_id                       bigint,
  constraint ck_login_audit_trail_event_type check (event_type in ('LOGIN_OK','USER_NOT_FOUND','USER_NOT_ENABLED','USER_NOT_VERIFIED','LOGOUT_OK')),
  constraint pk_login_audit_trail primary key (id)
);

create table payment (
  dtype                         varchar(10) not null,
  id                            bigint auto_increment not null,
  amount                        decimal(38),
  payment_method                integer,
  payment_date                  datetime(6),
  payment_reference             varchar(255),
  received_by_id                bigint,
  reservation_id                bigint,
  constraint ck_payment_payment_method check (payment_method in (0,1,2)),
  constraint uq_payment_reservation_id unique (reservation_id),
  constraint pk_payment primary key (id)
);

create table reservation (
  id                            bigint auto_increment not null,
  customer_id                   bigint,
  building_id                   bigint,
  start_date                    datetime(6),
  end_date                      datetime(6),
  reservation_date              datetime(6),
  constraint pk_reservation primary key (id)
);

create table reservation_history (
  id                            bigint auto_increment not null,
  reservation_id                bigint,
  status                        integer,
  change_date                   datetime(6),
  staff_id                      bigint,
  constraint ck_reservation_history_status check (status in (0,1,2)),
  constraint pk_reservation_history primary key (id)
);

create table role (
  id                            bigint auto_increment not null,
  role_name                     varchar(18),
  role_description              varchar(255),
  constraint ck_role_role_name check (role_name in ('CREATE_USER','EDIT_USER','VIEW_USER','CREATE_RESERVATION','EDIT_RESERVATION','VIEW_RESERVATION','CREATE_BUILDING','EDIT_BUILDING','VIEW_BUILDING','CREATE_CUSTOMER','EDIT_CUSTOMER','VIEW_CUSTOMER')),
  constraint pk_role primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  first_name                    varchar(255),
  middle_name                   varchar(255),
  last_name                     varchar(255),
  phone_number                  varchar(255),
  staff_number                  varchar(255),
  username                      varchar(255),
  password_hash                 varchar(255),
  is_enabled                    tinyint(1) default 0,
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id)
);

create table user_role (
  user_id                       bigint not null,
  role_id                       bigint not null,
  constraint pk_user_role primary key (user_id,role_id)
);

alter table building_facility add constraint fk_building_facility_building foreign key (building_id) references building (id) on delete restrict on update restrict;
create index ix_building_facility_building on building_facility (building_id);

alter table building_facility add constraint fk_building_facility_facility foreign key (facility_id) references facility (id) on delete restrict on update restrict;
create index ix_building_facility_facility on building_facility (facility_id);

alter table login_audit_trail add constraint fk_login_audit_trail_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_login_audit_trail_user_id on login_audit_trail (user_id);

alter table payment add constraint fk_payment_received_by_id foreign key (received_by_id) references user (id) on delete restrict on update restrict;
create index ix_payment_received_by_id on payment (received_by_id);

alter table payment add constraint fk_payment_reservation_id foreign key (reservation_id) references reservation (id) on delete restrict on update restrict;

alter table reservation add constraint fk_reservation_customer_id foreign key (customer_id) references customer (id) on delete restrict on update restrict;
create index ix_reservation_customer_id on reservation (customer_id);

alter table reservation add constraint fk_reservation_building_id foreign key (building_id) references building (id) on delete restrict on update restrict;
create index ix_reservation_building_id on reservation (building_id);

alter table reservation_history add constraint fk_reservation_history_reservation_id foreign key (reservation_id) references reservation (id) on delete restrict on update restrict;
create index ix_reservation_history_reservation_id on reservation_history (reservation_id);

alter table reservation_history add constraint fk_reservation_history_staff_id foreign key (staff_id) references user (id) on delete restrict on update restrict;
create index ix_reservation_history_staff_id on reservation_history (staff_id);

alter table user_role add constraint fk_user_role_user foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_user_role_user on user_role (user_id);

alter table user_role add constraint fk_user_role_role foreign key (role_id) references role (id) on delete restrict on update restrict;
create index ix_user_role_role on user_role (role_id);


# --- !Downs

alter table building_facility drop foreign key fk_building_facility_building;
drop index ix_building_facility_building on building_facility;

alter table building_facility drop foreign key fk_building_facility_facility;
drop index ix_building_facility_facility on building_facility;

alter table login_audit_trail drop foreign key fk_login_audit_trail_user_id;
drop index ix_login_audit_trail_user_id on login_audit_trail;

alter table payment drop foreign key fk_payment_received_by_id;
drop index ix_payment_received_by_id on payment;

alter table payment drop foreign key fk_payment_reservation_id;

alter table reservation drop foreign key fk_reservation_customer_id;
drop index ix_reservation_customer_id on reservation;

alter table reservation drop foreign key fk_reservation_building_id;
drop index ix_reservation_building_id on reservation;

alter table reservation_history drop foreign key fk_reservation_history_reservation_id;
drop index ix_reservation_history_reservation_id on reservation_history;

alter table reservation_history drop foreign key fk_reservation_history_staff_id;
drop index ix_reservation_history_staff_id on reservation_history;

alter table user_role drop foreign key fk_user_role_user;
drop index ix_user_role_user on user_role;

alter table user_role drop foreign key fk_user_role_role;
drop index ix_user_role_role on user_role;

drop table if exists building;

drop table if exists building_facility;

drop table if exists customer;

drop table if exists facility;

drop table if exists login_audit_trail;

drop table if exists payment;

drop table if exists reservation;

drop table if exists reservation_history;

drop table if exists role;

drop table if exists user;

drop table if exists user_role;

