package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class Building extends Model {
    @Id
    public long id;
    public String buildingName;
    public String buildingAddress;

    @OneToMany
    public List<Reservation> reservations;


    public static Finder<Long, Building> find = new Finder<>(Building.class);

    @ManyToMany
    public List<Facility> facilities;

    @Override
    public String toString() {
        return "Building{" +
                "id=" + id +
                ", buildingName='" + buildingName + '\'' +
                ", buildingAddress='" + buildingAddress + '\'' +
                '}';
    }


}
