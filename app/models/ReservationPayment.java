package models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * @author adewaleafolabi
 */
@Entity
@DiscriminatorValue("RESERVATION")
public class ReservationPayment extends Payment {
    @OneToOne
    public  Reservation reservation;

}
