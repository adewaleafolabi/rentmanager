package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class Reservation extends Model{

    @Id
    public long id;
    @ManyToOne
    public Customer customer;
    @ManyToOne
    public Building building;
    public Date startDate;
    public Date endDate;
    public Date reservationDate;

    public Reservation() {
        this.customer = new Customer();
        this.building = new Building();
        this.reservationDate = new Date();
    }



    @OneToMany
    public List<ReservationHistory> history;

    public static Finder<Long, Reservation> find = new Finder<>(Reservation.class);

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", customer=" + customer +
                ", building=" + building +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", reservationDate=" + reservationDate +
                "} " + super.toString();
    }
}
