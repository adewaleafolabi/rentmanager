package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * @author adewaleafolabi
 */
@Entity
public class ReservationHistory {

    @Id
    public long id;
    @ManyToOne
    public Reservation reservation;
    public ReservationStatus status;
    public Date changeDate;
    @ManyToOne
    public User staff;

}
