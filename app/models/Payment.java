package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author adewaleafolabi
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Payment extends Model {
    @Id
    public long id;
    public BigDecimal amount;
    public PaymentMethod paymentMethod;
    public Date paymentDate;
    public String paymentReference;
    @ManyToOne
    public User receivedBy;


}
