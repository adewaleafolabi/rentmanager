package models;

/**
 * @author adewaleafolabi
 */
public enum PaymentMethod {
    CASH,
    BANK_DEPOSIT,
    ONLINE_PAY
}
