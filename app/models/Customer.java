package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Optional;

/**
 * @author adewaleafolabi
 */
@Entity
public class Customer extends Model{
    @Id
    public long id;
    public String firstName;
    public String middleName;
    public String lastName;
    public String phoneNumber;
    @Constraints.Email
    public String email;

    public static Finder<Long, Customer> find = new Finder<>(Customer.class);

    @OneToMany
    public List<Reservation> reservations;

    public String fullName(){
        return String.format("%s %s %s",Optional.ofNullable(firstName).orElse(""),Optional.ofNullable(middleName).orElse(""),Optional.ofNullable(lastName).orElse(""));
    }


    public String display(){
        return String.format("%s %s %s - %s -%s",Optional.ofNullable(firstName).orElse(""),Optional.ofNullable(middleName).orElse(""),Optional.ofNullable(lastName).orElse(""),Optional.ofNullable(phoneNumber).orElse(""),Optional.ofNullable(email).orElse(""));
    }


    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
