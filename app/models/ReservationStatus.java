package models;

/**
 * @author adewaleafolabi
 */
public enum ReservationStatus {
    PENDING,
    COMPLETED,
    CANCELLED
}
