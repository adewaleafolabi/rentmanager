package models.forms;

import play.data.format.Formats;

import java.util.Date;

/**
 * @author adewaleafolabi
 */
public class ReservationForm {

    public long id;
    public long customerID;
    public long buildingID;
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date startDate;
    @Formats.DateTime(pattern="yyyy-MM-dd")
    public Date endDate;

}
