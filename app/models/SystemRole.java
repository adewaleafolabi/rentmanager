package models;

/**
 * @author adewaleafolabi
 */
public enum SystemRole {
    CREATE_USER,
    EDIT_USER,
    VIEW_USER,

    CREATE_RESERVATION,
    EDIT_RESERVATION,
    VIEW_RESERVATION,

    CREATE_BUILDING,
    EDIT_BUILDING,
    VIEW_BUILDING,

    CREATE_CUSTOMER,
    EDIT_CUSTOMER,
    VIEW_CUSTOMER

}
