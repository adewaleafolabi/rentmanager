package services;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.ClassUtils;
import play.libs.Json;
import play.mvc.Http;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author adewaleafolabi
 */
public class DataTable {
    final String DATE_FORMAT ="yyyy-MM-dd HH:mm:ss";
    List<Class> excluded = new ArrayList<>();

    public DataTable() {
        excluded.add(String.class);
        excluded.add(BigDecimal.class);
        excluded.add(Date.class);
    }

    private <T> ObjectNode generateRow(T item, List<String> columns) {

        item.toString();
        List<Class> excluded = Arrays.asList(String.class,Date.class, BigDecimal.class,Enum.class,Timestamp.class);

        ObjectNode output = Json.newObject();


        try {

            Field[] fields = item.getClass().getFields();

            for (int i = 0; i < columns.size(); i++) {
                String s = columns.get(i);
                final String lookUp;
                final String subLookUp;
                if (s.contains(".")) {
                    String[] entry = s.split("\\.");
                    lookUp = entry[0];
                    subLookUp = entry[1];
                } else {
                    lookUp = s;
                    subLookUp = "";
                    final String counterValue = String.valueOf(i);
                    Arrays.stream(fields).filter(field -> field.getName().equalsIgnoreCase(lookUp)).findFirst().ifPresent(field1 -> {
                        output.put(counterValue, field1.toString());
                    });
                }

                final String counterValue = String.valueOf(i);
                Optional<Field> optionalField = Arrays.stream(fields).filter(field1 -> field1.getName().equalsIgnoreCase(lookUp)).findFirst();
                if (optionalField.isPresent()) {

                    final String outputEntry = "";
                    Field field = optionalField.get();
                    Object o = Optional.ofNullable(field.get(item)).orElse("");
                    if (!ClassUtils.isPrimitiveOrWrapper(o.getClass()) && !(excluded.contains(o.getClass()) || excluded.contains(o.getClass().getSuperclass())  ) ) {
                        Optional<Field> optionalSubField = Arrays.stream(o.getClass().getFields()).filter(field1 -> field1.getName().equalsIgnoreCase(subLookUp)).findFirst();

                        if (optionalSubField.isPresent()) {
                            Field field2 = optionalSubField.get();
                            field2.setAccessible(true);
                            try {
                                Object value = Optional.ofNullable(field2.get(o)).orElse("");
                                if(o.getClass().equals(java.sql.Timestamp.class)){
                                    output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format(Date.from(((Timestamp) value).toInstant())));
                                }else if(o.getClass().getSuperclass().equals(Enum.class)){
                                    output.put(String.valueOf(i), ((Enum) value).name());
                                }
                                else if(value.getClass().equals(java.util.Date.class)){
                                    output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format((Date)value));
                                }else{
                                    output.put(String.valueOf(i), value.toString());
                                }

                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Object value = Optional.ofNullable(o).orElse("");
                        if(o.getClass().equals(java.sql.Timestamp.class)){
                            output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format(Date.from(((Timestamp) value).toInstant())));
                        }
                        else if(o.getClass().getSuperclass().equals(Enum.class)){
                            output.put(String.valueOf(i), ((Enum) value).name());
                        }
                        else if(o.getClass().equals(java.util.Date.class)){
                            output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format((Date) value));
                        }else{
                            output.put(String.valueOf(i), value.toString());
                        }

                    }


                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
/*
    private <T> ObjectNode generateRow(T item, List<String> columns) {

        ObjectNode output = Json.newObject();


        try {

            Field[] fields = item.getClass().getFields();

            for (int i = 0; i < columns.size(); i++) {
                String s = columns.get(i);
                final String lookUp;
                final String subLookUp;
                if (s.contains(".")) {
                    String[] entry = s.split("\\.");
                    lookUp = entry[0];
                    subLookUp = entry[1];
                } else {
                    lookUp = s;
                    subLookUp = "";
//                    final String counterValue = String.valueOf(i);
//                    Arrays.stream(fields).filter(field -> field.getName().equalsIgnoreCase(lookUp)).findFirst().ifPresent(field1 -> {
//                        output.put(counterValue, field1.toString());
//                    });
                }

                final String counterValue = String.valueOf(i);
                Optional<Field> optionalField = Arrays.stream(fields).filter(field1 -> field1.getName().equalsIgnoreCase(lookUp)).findFirst();
                if (optionalField.isPresent()) {

                    final String outputEntry = "";
                    Field field = optionalField.get();
                    Object o = field.get(item);
                    if ((o != null)) {
                        if (!ClassUtils.isPrimitiveOrWrapper(o.getClass()) && !excluded.contains(o.getClass())) {
                            Optional<Field> optionalSubField = Arrays.stream(o.getClass().getFields()).filter(field1 -> field1.getName().equalsIgnoreCase(subLookUp)).findFirst();

                            if (optionalSubField.isPresent()) {
                                System.out.println("SubField is present");
                                Field field2 = optionalSubField.get();
                                field2.setAccessible(true);
                                try {
                                    Object value = field2.get(o);

                                    System.out.println(value.getClass().getName());

                                    if(value.getClass().equals(java.util.Date.class)){
                                        output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format((Date) value));
                                    }else {
                                        output.put(String.valueOf(i),value.toString());
                                    }

                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            if(o.getClass().equals(java.util.Date.class)){
                                output.put(String.valueOf(i), new SimpleDateFormat(DATE_FORMAT).format((Date) o));
                            }else{
                                output.put(String.valueOf(i), o.toString());
                            }

                        }
                    }else {
                        output.put(String.valueOf(i), "");

                    }

                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
*/
    public <T> ObjectNode generate(Model.Finder<Long, T> finder, List<String> columns, Http.Request request) throws NoSuchFieldException, IllegalAccessException {
        Integer iTotalRecords = finder.findRowCount();
        ExpressionList<T> expressionList = finder.where();


        int length = Optional.ofNullable(request.getQueryString("length")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int start = Optional.ofNullable(request.getQueryString("start")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int orderColumn = Optional.ofNullable(request.getQueryString("order[0][column]")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int draw = Optional.ofNullable(request.getQueryString("draw")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        Integer pageSize = length;
        Integer page = start / length;

        String sortBy = columns.get(orderColumn);

        String order = Optional.ofNullable(request.getQueryString("order[0][dir]")).orElse("asc");

        String GLOBAL_SEARCH_TERM = Optional.ofNullable(request.getQueryString("search[value]")).orElse("");

        for (int i = 0; i < columns.size(); i++) {
            String columnSearchValue = Optional.ofNullable(request.getQueryString(String.format("columns[%s][search][value]", i))).orElse("");

            if (!columnSearchValue.isEmpty()) {
                expressionList = expressionList.ilike(columns.get(i), columnSearchValue + "%");

            }
        }


        PagedList<T> pagedList = expressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = pagedList.getTotalRowCount();

        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (T item : pagedList.getList()) {
            ObjectNode row = Json.newObject();


            an.add(generateRow(item, columns));


        }

        return result;

    }




    public <T> ObjectNode generate(ExpressionList<T> finder, List<String> columns, Http.Request request) throws NoSuchFieldException, IllegalAccessException {
        Integer iTotalRecords = finder.findRowCount();
        ExpressionList<T> expressionList = finder;

        int length = Optional.ofNullable(request.getQueryString("length")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int start = Optional.ofNullable(request.getQueryString("start")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int orderColumn = Optional.ofNullable(request.getQueryString("order[0][column]")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int draw = Optional.ofNullable(request.getQueryString("draw")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        Integer pageSize = length;
        Integer page = start / length;

        String sortBy = columns.get(orderColumn);

        String order = Optional.ofNullable(request.getQueryString("order[0][dir]")).orElse("asc");

        String GLOBAL_SEARCH_TERM = Optional.ofNullable(request.getQueryString("search[value]")).orElse("");

        for (int i = 0; i < columns.size(); i++) {
            String columnSearchValue = Optional.ofNullable(request.getQueryString(String.format("columns[%s][search][value]", i))).orElse("");

            if (!columnSearchValue.isEmpty()) {
                expressionList = expressionList.ilike(columns.get(i), columnSearchValue + "%");

            }
        }


        PagedList<T> pagedList = expressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = pagedList.getTotalRowCount();

        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (T item : pagedList.getList()) {
            ObjectNode row = Json.newObject();


            an.add(generateRow(item, columns));


        }

        return result;

    }
}
