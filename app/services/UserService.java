package services;

import models.Role;
import models.User;
import models.forms.UserCreationForm;
import play.Logger;
import security.Hash;
import vo.ServiceResponse;

import java.util.Collections;

/**
 * @author adewaleafolabi
 */
public class UserService {

    public ServiceResponse createUser(UserCreationForm userCreationFormData){

        userCreationFormData.roles.removeAll(Collections.singleton(null));
        User user = new User();
        user.isEnabled = true;
        user.username = userCreationFormData.username;

        userCreationFormData.roles.stream().distinct().forEach(i -> {
            user.roles.add(Role.find.byId(i));
        });


        try {
            user.passwordHash = Hash.createPassword(userCreationFormData.password);
            user.save();

        } catch (Exception e) {
            Logger.error("Problem in saving user {}", e);
            return new ServiceResponse(ServiceResponse.ERROR,ServiceResponse.GENERAL_FAILURE_MESSAGE);
        }

        return new ServiceResponse(ServiceResponse.SUCCESS,ServiceResponse.USER_CREATION_OK);
    }
}
