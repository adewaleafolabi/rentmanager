import play.http.HttpErrorHandler;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * @author adewaleafolabi
 */
public class ErrorHandler implements HttpErrorHandler {
    @Override
    public CompletionStage<Result> onClientError(Http.RequestHeader requestHeader, int statusCode, String message) {
        play.Logger.debug(requestHeader.path());
        return CompletableFuture.completedFuture(
                Results.status(statusCode, "A client error occurred: " + message)
        );
    }

    @Override
    public CompletionStage<Result> onServerError(Http.RequestHeader requestHeader, Throwable exception) {
        play.Logger.error("Server error {}",exception);
        return CompletableFuture.completedFuture(
                Results.internalServerError("A server error occurred: " + exception.getMessage())
        );
    }
}
