package controllers;

import models.Building;
import models.Customer;
import models.Reservation;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author adewaleafolabi
 */
public class DashboardController extends Controller {

    public Result index(){
        List<Reservation> reservations = Reservation.find.all();
        Map<String, Integer> stats = new HashMap<>();
        stats.put("buildings", Building.find.findRowCount());
        stats.put("staffs", User.find.findRowCount());
        stats.put("customers", Customer.find.findRowCount());
        stats.put("reservations", reservations.size());
        return ok(views.html.dashboard.index.render(stats,reservations));
    }



}
