package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Expression;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Building;
import models.Customer;
import models.Reservation;
import models.SystemRole;
import models.forms.ReservationForm;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;
import services.DataTable;
import vo.Calendar;
import vo.ServiceResponse;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author adewaleafolabi
 */
public class ReservationController extends Controller {
    @Inject
    DataTable dataTable;

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result index() {
        return ok(views.html.reservation.index.render());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_RESERVATION)
    public Result create() {
        return ok(views.html.reservation.create.render(formFactory.form(ReservationForm.class), Customer.find.all(), Building.find.all()));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_RESERVATION)
    public Result edit(final long id) {
        Reservation reservation = Reservation.find.byId(id);

        if(reservation==null){

            flash("warning", ServiceResponse.RESERVATION_NOT_FOUND);
            return redirect(controllers.routes.ReservationController.index());
        }

        ReservationForm reservationForm = new ReservationForm();
        reservationForm.buildingID=reservation.building.id;
        reservationForm.customerID=reservation.customer.id;
        reservationForm.endDate=reservation.endDate;
        reservationForm.startDate=reservation.startDate;
        reservationForm.id=reservation.id;
        Form<ReservationForm> editForm = formFactory.form(ReservationForm.class).fill(reservationForm);

        return ok(views.html.reservation.edit.render(editForm, Customer.find.all(), Building.find.all()));
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_RESERVATION)
    public Result save() {
        Form<ReservationForm> creationForm = formFactory.form(ReservationForm.class).bindFromRequest();

        if (creationForm.hasErrors()) {

            play.Logger.error(creationForm.errorsAsJson().asText());
            return badRequest(views.html.reservation.create.render(creationForm, Customer.find.all(), Building.find.all()));

        }

        ReservationForm data = creationForm.get();

        try {
            Reservation reservation = new Reservation();
            reservation.endDate = data.endDate;
            reservation.startDate = data.startDate;
            reservation.customer = new Customer();
            reservation.customer.id = data.customerID;
            reservation.building = new Building();
            reservation.building.id = data.buildingID;

            reservation.save();

        } catch (Exception e) {
            play.Logger.error("Error while persisting reservation {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.reservation.create.render(creationForm, Customer.find.all(), Building.find.all()));

        }

        flash("success", ServiceResponse.RESERVATION_CREATION_OK);

        return redirect(controllers.routes.ReservationController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_RESERVATION)
    public Result update() {
        Form<ReservationForm> editForm = formFactory.form(ReservationForm.class).bindFromRequest();

        if (editForm.hasErrors()) {
            return badRequest(views.html.reservation.edit.render(editForm, Customer.find.all(), Building.find.all()));
        }

        ReservationForm data = editForm.get();

        Reservation reservation = Reservation.find.byId(data.id);
        reservation.endDate = data.endDate;
        reservation.startDate = data.startDate;
        reservation.customer = new Customer();
        reservation.customer.id = data.customerID;
        reservation.building = new Building();
        reservation.building.id = data.buildingID;

        try {
            reservation.update();

        } catch (Exception e) {
            play.Logger.error("Error while updating reservation {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.reservation.edit.render(editForm, Customer.find.all(), Building.find.all()));

        }

        flash("success", ServiceResponse.BUILDING_UPDATE_OK);

        return redirect(controllers.routes.ReservationController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RESERVATION)
    public Result view(final long id) {
        Reservation reservation = Reservation.find.byId(id);

        if(reservation==null){

            flash("warning", ServiceResponse.RESERVATION_NOT_FOUND);
            return redirect(controllers.routes.ReservationController.index());
        }

        return ok(views.html.reservation.view.render(reservation));

    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RESERVATION)
    public Result dataTable() {

        ObjectNode result;
        try {
            result = dataTable.generate(Reservation.find, Arrays.asList("customer.firstName", "customer.middleName", "customer.lastName", "customer.phoneNumber", "customer.id", "building.buildingName", "building.id", "startDate", "endDate", "reservationDate", "id"), request());
        } catch (Exception e) {
            play.Logger.error("DataTable error {}", e);
            result = Json.newObject();
        }

        return ok(result);
    }


    public Result receipt(final long id) {
        Reservation reservation = Reservation.find.byId(id);

        if (reservation == null) {

            flash("warning", ServiceResponse.RESERVATION_NOT_FOUND);
            return redirect(controllers.routes.ReservationController.index());
        }

        return ok(views.html.invoice.index.render(reservation));

    }


    public Result reservationForCalendar(){

        DynamicForm requestData = formFactory.form().bindFromRequest();
        String startDate = requestData.get("start");
        String endDate = requestData.get("end");

        List<Calendar> calendars = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Reservation.find.where().ge("startDate", startDate).le("endDate", endDate).findList().forEach(reservation -> {
            calendars.add(new Calendar(reservation.id, reservation.customer.fullName(), df.format(reservation.startDate), df.format(reservation.endDate)));
        });

        return ok(Json.toJson(calendars));
    }
}
