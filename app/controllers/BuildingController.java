package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Building;
import models.SystemRole;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;
import services.DataTable;
import vo.ServiceResponse;

import javax.inject.Inject;
import java.util.Arrays;

/**
 * @author adewaleafolabi
 */
public class BuildingController extends Controller {
    @Inject
    DataTable dataTable;

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_BUILDING)
    public Result index() {

        return ok(views.html.building.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_BUILDING)
    public Result create() {

        return ok(views.html.building.create.render(formFactory.form(Building.class)));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_BUILDING)
    public Result edit(final long id) {
        Building building = Building.find.byId(id);

        if(building==null){

            flash("warning", ServiceResponse.BUILDING_NOT_FOUND);
            return redirect(controllers.routes.BuildingController.index());
        }

        return ok(views.html.building.edit.render(formFactory.form(Building.class).fill(building)));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_BUILDING)
    public Result view(final long id) {
        Building building = Building.find.byId(id);

        if(building==null){

            flash("warning", ServiceResponse.BUILDING_NOT_FOUND);
            return redirect(controllers.routes.BuildingController.index());
        }

        return ok(views.html.building.view.render(building));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_BUILDING)
    public Result save() {

        Form<Building> creationForm = formFactory.form(Building.class).bindFromRequest();

        if (creationForm.hasErrors()) {
            return badRequest(views.html.building.create.render(creationForm));

        }

        Building building = creationForm.get();

        try {
            building.save();

        } catch (Exception e) {
            play.Logger.error("Error while persisting building {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.building.create.render(creationForm));
        }

        flash("success", ServiceResponse.BUILDING_CREATION_OK);

        return redirect(controllers.routes.BuildingController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_BUILDING)
    public Result update() {
        Form<Building> editForm = formFactory.form(Building.class).bindFromRequest();

        if (editForm.hasErrors()) {
            return badRequest(views.html.building.edit.render(editForm));

        }

        Building building = editForm.get();

        try {
            building.update();

        } catch (Exception e) {
            play.Logger.error("Error while persisting building {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.building.edit.render(editForm));
        }

        flash("success", ServiceResponse.BUILDING_UPDATE_OK);

        return redirect(controllers.routes.BuildingController.index());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_BUILDING)
    public Result dataTable() {

        ObjectNode result;
        try {
            result = dataTable.generate(Building.find, Arrays.asList("id", "buildingName", "buildingAddress"), request());
        } catch (Exception e) {
            play.Logger.error("DataTable error {}", e);
            result = Json.newObject();
        }

        return ok(result);
    }
}
