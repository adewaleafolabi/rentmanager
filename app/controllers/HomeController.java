package controllers;

import models.User;
import models.audit.Login;
import models.audit.LoginResult;
import models.forms.LoginForm;
import play.Logger;
import play.cache.Cache;
import play.cache.CacheApi;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import security.HostMaster;

import javax.inject.Inject;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    @Inject
    HostMaster hostMaster;
    @Inject
    CacheApi cache;

    @Inject
    FormFactory formFactory;

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        Form<LoginForm> loginForm = formFactory.form(LoginForm.class).bindFromRequest();
        return ok(views.html.login.render(loginForm));
    }


    public Result login() {
        Form<LoginForm> loginForm = formFactory.form(LoginForm.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            play.Logger.debug(loginForm.errorsAsJson().asText());
            return ok(views.html.login.render(loginForm));
        }

        session("auth_user_name", loginForm.get().username);
        return redirect(routes.DashboardController.index());

    }


    public Result logout() {
        Logger.debug("Logout initiated");

        User currentUser = hostMaster.getCurrentUser();

        if (currentUser == null) {

            Logger.info("No user currently logged in");

        } else {

            cache.remove(currentUser.username + "_auth_user");

            session().clear();

            Logger.info("Cache and Session cleared");

            try {
                Login loginTrail = new Login(LoginResult.LOGOUT_OK, request().remoteAddress(), currentUser);

                loginTrail.save();

            } catch (Exception e) {

                play.Logger.error("failed to save audit trail {}", e);

            }

        }

        Logger.info("user ended session ");

        flash("success", "Logout Successful");


        return redirect(controllers.routes.HomeController.index());
    }


}
