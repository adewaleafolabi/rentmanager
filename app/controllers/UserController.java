package controllers;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Role;
import models.SystemRole;
import models.User;
import models.forms.UserCreationForm;
import org.apache.commons.lang3.RandomStringUtils;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import scala.concurrent.ExecutionContextExecutor;
import security.AppException;
import security.Hash;
import security.RoleSecured;
import security.Secured;
import services.DataTable;
import services.UserService;
import views.html.user.create;
import vo.ServiceResponse;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * @author adewaleafolabi
 */
public class UserController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;

    @Inject
    DataTable dataTable;

    @Inject
    UserService userService;

    @Inject
    FormFactory formFactory;

    @Inject
    public UserController(ActorSystem actorSystem, ExecutionContextExecutor exec) {
        this.actorSystem = actorSystem;
        this.exec = exec;
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result index() {
        return ok(views.html.user.index.render());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_USER)
    public Result save() {
        List<Role> roles = Role.find.all();

        final Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).bindFromRequest();

        if (userForm.hasErrors()) {
            return badRequest(create.render(userForm, roles));
        }

        ServiceResponse serviceResponse = userService.createUser(userForm.get());

        if (serviceResponse.responseCode == ServiceResponse.SUCCESS) {
            flash("success", serviceResponse.message);
            return redirect(controllers.routes.UserController.index());
        }

        flash("danger", serviceResponse.message);
        return badRequest(create.render(userForm, roles));
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_USER)
    public Result update() {
        String message = "";
        final Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).bindFromRequest();
        List<Role> roles = Role.find.all();


        if (userForm.hasErrors()) {
            return badRequest(views.html.user.edit.render(userForm, roles));
        }

        UserCreationForm userCreationData = userForm.get();

        User userFromDB = User.find.byId(userCreationData.id);

        if (userFromDB != null) {

            userFromDB.firstName = userCreationData.firstName;
            userFromDB.lastName = userCreationData.lastName;
            userFromDB.middleName = userCreationData.middleName;
            userFromDB.staffNumber = userCreationData.staffNumber;
            userFromDB.phoneNumber = userCreationData.phoneNumber;

            if (userCreationData.roles != null) {
                userFromDB.roles.clear();
                userCreationData.roles.stream().forEach(i -> {
                    userFromDB.roles.add(new Role(i));
                });
            }

            if (!Optional.ofNullable(userCreationData.password).orElse("").isEmpty()) {
                try {
                    userFromDB.passwordHash = Hash.createPassword(userCreationData.password);
                } catch (AppException e) {
                    play.Logger.error("Problem in updating user password {}", e);
                    message = "Problem encountered updating user password";
                    flash("warning", message);
                    return badRequest(views.html.user.edit.render(userForm, roles));

                }
            }

            try {
                userFromDB.update();
                flash("success", "User information updated");

                return redirect(controllers.routes.UserController.index());

            } catch (Exception e) {
                play.Logger.error("Problem in updating user {}", e);
                message = "Problem encountered updating user information";
                flash("warning", message);
                return badRequest(views.html.user.edit.render(userForm, roles));
            }

        }

        message = "User information not found";
        flash("warning", message);
        return badRequest(views.html.user.edit.render(userForm, roles));


    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_USER)
    public Result edit(long id) {
        User user = User.find.byId(id);

        if (user == null) {
            flash("danger", "User not found");
            return redirect(controllers.routes.UserController.index());
        }

        List<Role> systemRoles = Role.find.all();


        UserCreationForm userCreationFormData = new UserCreationForm();
        userCreationFormData.username = user.username;
        userCreationFormData.roles = user.roles.stream().map(role -> role.id).collect(Collectors.toList());
        userCreationFormData.firstName = user.firstName;
        userCreationFormData.middleName = user.middleName;
        userCreationFormData.lastName = user.lastName;
        userCreationFormData.staffNumber = user.staffNumber;
        userCreationFormData.phoneNumber = user.phoneNumber;
        userCreationFormData.id = user.id;


        final Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).fill(userCreationFormData);

        return ok(views.html.user.edit.render(userForm, systemRoles));


    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result view(long id) {

        User user = User.find.byId(id);

        if (user == null) {
            flash("danger", "User not found");
            return redirect(controllers.routes.UserController.index());
        }

        return ok(views.html.user.view.render(user, Role.find.all()));

    }

    @RoleSecured(role = SystemRole.CREATE_USER)
    public Result create() {

        UserCreationForm userCreationForm = new UserCreationForm();
        userCreationForm.password = RandomStringUtils.randomAlphanumeric(8);

        Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).fill(userCreationForm);

        return ok(views.html.user.create.render(userForm, Role.find.all()));
    }


    public Result updateRoles(){
        DynamicForm dynamicForm = formFactory.form().bindFromRequest();
        String userID = dynamicForm.get("userID");
        User user = User.find.where().eq("id",userID).findUnique();
        ObjectNode message = Json.newObject();
        if(user==null){
            message.put("message",ServiceResponse.USER_NOT_FOUND);

            return (ok(message));

        }

        dynamicForm.get


    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result dataTable() {

        ObjectNode result;
        try {
            result = dataTable.generate(User.find, Arrays.asList("firstName", "middleName", "lastName", "phoneNumber", "staffNumber", "username", "isEnabled", "id"), request());
        } catch (Exception e) {
            play.Logger.error("DataTable error {}", e);
            result = Json.newObject();
        }

        return ok(result);
    }







}
