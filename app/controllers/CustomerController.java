package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Customer;
import models.SystemRole;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;
import services.DataTable;
import vo.ServiceResponse;

import javax.inject.Inject;
import java.util.Arrays;

/**
 * @author adewaleafolabi
 */
public class CustomerController extends Controller {

    @Inject
    DataTable dataTable;

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_CUSTOMER)
    public Result index() {

        return ok(views.html.customer.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_CUSTOMER)
    public Result create() {

        return ok(views.html.customer.create.render(formFactory.form(Customer.class)));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_CUSTOMER)
    public Result edit(final long id) {
        Customer customer = Customer.find.byId(id);

        if(customer==null){

            flash("warning", ServiceResponse.CUSTOMER_NOT_FOUND);
            return redirect(controllers.routes.CustomerController.index());
        }

        return ok(views.html.customer.edit.render(formFactory.form(Customer.class).fill(customer)));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_CUSTOMER)
    public Result view(final long id) {
        Customer customer = Customer.find.byId(id);

        if(customer==null){

            flash("warning", ServiceResponse.CUSTOMER_NOT_FOUND);
            return redirect(controllers.routes.CustomerController.index());
        }

        return ok(views.html.customer.view.render(customer));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_CUSTOMER)
    public Result save() {

        Form<Customer> creationForm = formFactory.form(Customer.class).bindFromRequest();

        if (creationForm.hasErrors()) {
            return badRequest(views.html.customer.create.render(creationForm));

        }

        Customer customer = creationForm.get();

        try {
            customer.save();

        } catch (Exception e) {
            play.Logger.error("Error while persisting customer {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.customer.create.render(creationForm));
        }

        flash("success", ServiceResponse.CUSTOMER_CREATION_OK);

        return redirect(controllers.routes.CustomerController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_CUSTOMER)
    public Result update() {
        Form<Customer> editForm = formFactory.form(Customer.class).bindFromRequest();

        if (editForm.hasErrors()) {
            return badRequest(views.html.customer.edit.render(editForm));

        }

        Customer customer = editForm.get();

        try {
            customer.update();

        } catch (Exception e) {
            play.Logger.error("Error while persisting customer {}", e);
            flash("warning", ServiceResponse.GENERAL_FAILURE_MESSAGE);
            return badRequest(views.html.customer.edit.render(editForm));
        }

        flash("success", ServiceResponse.CUSTOMER_UPDATE_OK);

        return redirect(controllers.routes.CustomerController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_CUSTOMER)
    public Result dataTable() {

        ObjectNode result;
        try {
            result = dataTable.generate(Customer.find, Arrays.asList("id", "firstName", "middleName","lastName","phoneNumber","email"), request());
        } catch (Exception e) {
            play.Logger.error("DataTable error {}", e);
            result = Json.newObject();
        }

        return ok(result);
    }
    
}
