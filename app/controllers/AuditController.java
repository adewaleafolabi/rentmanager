package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.SystemRole;
import models.audit.Login;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;
import services.DataTable;

import javax.inject.Inject;
import java.util.Arrays;

/**
 * Created by wale on 6/28/16.
 */
public class AuditController extends Controller{
    @Inject
    DataTable dataTable;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result loginAudit(final long id) {
        ObjectNode result;
        try {
            result = dataTable.generate(Login.find.where().eq("user.id",id), Arrays.asList("eventType", "eventDate", "ipAddress","id"), request());
        } catch (Exception e) {
            play.Logger.error("DataTable error {}", e);
            result = Json.newObject();
        }

        return ok(result);
    }
}
