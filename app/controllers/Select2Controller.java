package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * @author adewaleafolabi
 */
public class Select2Controller extends Controller {

    public Result index(){
        ObjectNode result = Json.newObject();
        ArrayNode an = result.putArray("items");
        User.find.all().stream().forEach(user -> an.add(Json.newObject().put("id", user.id).put("text", user.firstName)));
        return ok(result);
    }

    public Result test(){
        return ok(views.html.test.index.render());
    }
}
