package security;


import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.Date;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.session;

/**
 * @author adewaleafolabi
 */
public class Secured extends Security.Authenticator {
    @Inject
    play.Application playApp;

    @Override
    public String getUsername(Http.Context ctx) {

        // see if the session is expired
        String previousTick = session("userTime");

        if (previousTick != null && !previousTick.equals("")) {
            long previousT = Long.valueOf(previousTick);
            long currentT = new Date().getTime();
            long timeout = Long.valueOf(playApp.configuration().getString("sessionTimeout")) * 1000 * 60;
            if ((currentT - previousT) > timeout) {
                // session expired
                session().clear();
                return null;
            }
        }

        // update time in session
        String tickString = Long.toString(new Date().getTime());
        session("userTime", tickString);
        return ctx.session().get("auth_user_name");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        flash("danger", "kindly login first");
        play.Logger.debug("URL {}", ctx.request().uri().toString());
        session("dest_url", ctx.request().uri().toString());

        ctx.session().put("dest_url", ctx.request().uri().toString());

        Http.Context.current().session().put("dest_url", ctx.request().uri().toString());


        return redirect(controllers.routes.HomeController.index());
    }
}
