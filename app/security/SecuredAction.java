package security;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.session;

/**
 * @author adewaleafolabi
 */
public class SecuredAction extends Action.Simple {

    @Inject
    play.Application playApp;


    @Override
    public CompletionStage<Result> call(Http.Context context) {
        System.out.println("I am in SecuredAction");
        String previousTick = session("userTime");

        if (previousTick != null && !previousTick.equals("")) {
            long previousT = Long.valueOf(previousTick);
            long currentT = new Date().getTime();
            long timeout = Long.valueOf(playApp.configuration().getString("sessionTimeout")) * 1000 * 60;
            if ((currentT - previousT) > timeout) {
                session().clear();
                return CompletableFuture.completedFuture(
                        Results.unauthorized()
                );
            }
        }

        String tickString = Long.toString(new Date().getTime());
        session("userTime", tickString);
        System.out.println("All good");
        return delegate.call(context);
    }
}
